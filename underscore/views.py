from django.views.generic import ListView
from .models import Telegram_post


class TelegramListView(ListView):
    model = Telegram_post
    template_name = 'telegram_list.html'
