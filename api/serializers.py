from rest_framework import serializers
from underscore.models import Telegram_post


class TelegramSerializer(serializers.ModelSerializer):
    class Meta:
        model = Telegram_post
        fields = ('id', 'title',)
