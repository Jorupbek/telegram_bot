from rest_framework import generics
from underscore.models import Telegram_post
from .serializers import TelegramSerializer


class TelegramAPIView(generics.ListAPIView):
    queryset = Telegram_post.objects.all()
    serializer_class = TelegramSerializer


class TelegramAPICreate(generics.CreateAPIView):
    queryset = Telegram_post.objects.all()
    serializer_class = TelegramSerializer


class TelegramAPIDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Telegram_post.objects.all()
    serializer_class = TelegramSerializer
